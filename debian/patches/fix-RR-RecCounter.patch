Author: Dejan Latinovic <Dejan.Latinovic@rt-rk.com>
Description: Set RecCounter=0 if rbound_==0 to avoid cast of -inf.

https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=733917

Test test-det on i386 or amd64 successfully pass after a few iterations, on
mips this test has an enormous number of iterations and build of the linbox
package on buildd breaks after 300 minutes of inactivity.

The reason for this behavior is that in:

  linbox/algorithms/rational-reconstruction-base.h

the way to calculate RecCounter is:

  RecCounter = (size_t)((double)log((double)rbound_)/log(2.));//2^RecCounter < rbound_ <=2^(RecCounter+1)

When rbound_ is equal to zero the result of log function is -inf (double[8
bytes]).

When we cast -inf to size_t (unsigned int[4 bytes]) on i386 and amd64, the
result is zero.

In the same situation on mips architecture, cast is done through TRUNC.W.D
instruction.

For this instruction, if the source value is Infinity, NaN, or rounds to an
integer outside the range -2^31 to 2^31-1, the result cannot be represented
correctly and an IEEE Invalid Operation condition exists. The result depends on
the FP exception model currently active and the default result is 2^31-1
(2147483647).

--- a/linbox/algorithms/rational-reconstruction-base.h
+++ b/linbox/algorithms/rational-reconstruction-base.h
@@ -73,7 +73,8 @@
 				RecCounter = (size_t)sqrt((double)rbound_);//RecCounter^2 < rbound_ <=(RecCounter+1)^2
 			}
 			else if (_meth == GEOMETRIC) {
-				RecCounter = (size_t) log((double)rbound_) ;//2^RecCounter < rbound_ <=2^(RecCounter+1)
+				if (rbound_!=0)
+					RecCounter = (size_t) log((double)rbound_) ;//2^RecCounter < rbound_ <=2^(RecCounter+1)
 			}
 		}
 
@@ -85,7 +86,8 @@
 				RecCounter = (size_t)sqrt((double)rbound_);//RecCounter^2 < rbound_ <=(RecCounter+1)^2
 			}
 			else if (_meth == GEOMETRIC) {
-				RecCounter = (size_t)((double)log((double)rbound_)/log(2.));//2^RecCounter < rbound_ <=2^(RecCounter+1)
+				if (rbound_!=0)
+					RecCounter = (size_t)((double)log((double)rbound_)/log(2.));//2^RecCounter < rbound_ <=2^(RecCounter+1)
 			}
 
 		}
