linbox (1.7.0-4) UNRELEASED; urgency=medium

  * d/fix-the-size-formula-for-an-allocation.patch: apply proposed fix
    to resolve the buffer overrun (Closes: #1067787).

 -- Vladimir Petko <vladimir.petko@canonical.com>  Wed, 27 Mar 2024 08:23:18 +1300

linbox (1.7.0-3.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Rename libraries for 64-bit time_t transition.  Closes: #1062746

 -- Steve Langasek <vorlon@debian.org>  Fri, 01 Mar 2024 07:37:06 +0000

linbox (1.7.0-3) unstable; urgency=medium

  * debian/control
    - Bump Standards-Version to 4.6.2.
    - Add Multi-Arch field for liblinbox-doc (foreign).
  * debian/copyright
    - Update my copyright years.
  * debian/gbp.conf
    - New file; set debian-branch to debian/latest as recommended by
      DEP-14.
  * debian/watch
    - Switch from GitHub releases to tags page for download link.

 -- Doug Torrance <dtorrance@debian.org>  Sat, 07 Jan 2023 18:28:28 -0500

linbox (1.7.0-2) unstable; urgency=medium

  * debian/control
    - Add libfplll-dev to liblinbox-dev's Depends (Closes: #1001878).

 -- Doug Torrance <dtorrance@debian.org>  Sat, 18 Dec 2021 10:35:00 -0500

linbox (1.7.0-1) unstable; urgency=medium

  * Upload to unstable.

 -- Doug Torrance <dtorrance@debian.org>  Thu, 16 Dec 2021 16:18:23 -0500

linbox (1.7.0-1~exp1) experimental; urgency=medium

  * New upstream release.
  * debian/{control,copyright}
    - Update my email address; now a Debian Developer.
  * debian/control
    - Bump versioned dependencies on fflas-ffpack to 2.5.0.
    - Add libfplll-dev and libflint-dev to Build-Depends.
    - Drop libgivaro-dev from Build-Depends since it's a dependency of
      fflas-ffpack.
    - Rename liblinbox-1.6.3-0 binary package to liblinbox-1.7.0-0.
  * debian/liblinbox-1.7.0-0.install
    - Rename from liblinbox-1.6.3-0.install.
  * debian/patches
    - Remove patches applied upstream.
      + iterator-difference-type.patch
      + pkgconfig.patch
      + replace-dangerous-pointer-casts-with-memcpy.patch
      + reproducible-linbox-config.patch
  * debian/patches/skip-test-fft-modular-extended.patch
    - New patch; skip Test FFT with ModularExtended; failing on i386.
  * debian/patches/soname.patch
    - Refresh for new upstream release.  Also use autoconf's VERSION output
      variable to simplify future maintenance.
  * debian/patches/uint128.patch
    - New patch; only register uint128_t as a TypeName when it's available.
      Otherwise, test-fft will fail when it isn't.
  * debian/rules
    - Remove override_dh_autoreconf target skipping failing tests on MIPS
      architectures; possibly fixed upstream.
    - Drop redundant --prefix and --libdir options from dh_auto_configure.
    - Remove various configure options which are no longer available
      (enabling libraries and disabling instruction sets).
    - Drop --with-ntl, --with-iml, and --enable-shared from configure
      options; these are already the defaults.
    - Remove --without-fplll from configure options; we now build with
      fplll support.
    - Pass --without-archnative to configure to disable -march=native/
      -mcpu=native flags.
    - Use DEB_VERSION_UPSTREAM in override_dh_auto_install target instead
      of hardcoding the version number for simpler maintenance.
    - Stop fixing documentation version number in override_dh_auto_clean;
      no longer an issue.
    - New override_dh_clean target; don't remove
      interfaces/maple/lb-maple.mpl.bak; under version control.

 -- Doug Torrance <dtorrance@debian.org>  Tue, 14 Dec 2021 20:53:25 -0500

linbox (1.6.3-5) unstable; urgency=medium

  * debian/tests/control
    - Add allow-stderr restriction; warning messages on i386 were causing
      the tests to fail.

 -- Doug Torrance <dtorrance@piedmont.edu>  Mon, 15 Nov 2021 22:45:41 -0500

linbox (1.6.3-4) unstable; urgency=medium

  * debian/compat
    - Remove file; will build-depend on debhelper-compat instead.
  * debian/control
    - Update Maintainer to Debian Math Team.
    - Use debhelper-compat in Build-Depends and bump debhelper
      compatibility level to 13.
    - Add doxygen (for dh_doxygen) and jdupes to Build-Depends-Indep.
    - Bump Standards-Version to 4.6.0.
    - Update Vcs-* fields (science-team -> math-team).
    - Add Rules-Requires-Root field.
  * debian/copyright
    - Convert tabs to spaces.
    - Update file wildcards.
    - Add myself to copyright holders for debian/*.
  * debian/*.install
    - Drop unnecessary "debian/tmp" from paths; dh_install has looked
      there by default since debhelper 7.
  * debian/liblinbox-dev.examples
    - Include test matrices mentioned in examples/Readme.
  * debian/liblinbox-dev.manpages
    - Use installed copy of manpage so dh_missing doesn't complain.
  * debian/liblinbox-doc.lintian-overrides
    - Remove file; only override (embedded-javascript-library) was unused.
  * debian/not-installed
    - New file; tell dh_missing that libtool .la file is not installed.
  * debian/patches/reproducible-linbox-config.patch
    - New patch; backport a commit from upstream git which removes
      CXXFLAGS from the linbox-config script.  They contain the build
      path, which was causing non-reproducible builds.
  * debian/rules
    - Remove DH_VERBOSE comment.
    - Stop building developer's documentation; not installed.
    - Use jdupes to create symlinks to duplicate images in docs.
    - Run dh_doxygen to remove Doxygen cruft.
    - New override_dh_installexamples target.  Exclude Makefiles from
      example directories; they are generated by autotools and result
      in non-reproducible builds.
  * debian/salsa-ci.yml
    - Add Salsa pipelines config file.
  * debian/tests/control
    - Add simple smoke test for autopkgtest; compile and run one of the
      examples.
  * debian/upstream/metadata
    - Add DEP-12 upstream metadata file.

 -- Doug Torrance <dtorrance@piedmont.edu>  Thu, 11 Nov 2021 05:56:29 -0500

linbox (1.6.3-3) unstable; urgency=medium

  * debian/patches/iterator-difference-type.patch
    - New patch; use std::ptrdiff_t for vector iterator difference
      type (Closes: #987921).

 -- Doug Torrance <dtorrance@piedmont.edu>  Sun, 09 May 2021 12:28:04 -0400

linbox (1.6.3-2) unstable; urgency=medium

  * Team upload.

  [ Peter Michael Green ]
  * Replace dangerous pointer casts that are causing bus errors on
    some arm setups with memcpy calls (Closes: #944055).
  * Disable test-rank-md, test-solve-full and test-solve  on mips/mipsel,
    they fail to build with an out of memory error from the assembler on mipsel
    (Closes: #950433).
  * Fix clean target.

 -- Tobias Hansen <thansen@debian.org>  Sat, 01 Feb 2020 21:09:26 +0100

linbox (1.6.3-1) unstable; urgency=medium

  * Team upload.

  [ Doug Torrance ]
  * debian/control
    - Bump Standards-Version to 4.2.1.
  * debian/rules
    - Drop explicit call to dpkg-buildflags; not necessary after debhelper 9.
    - Include architecture.mk instead of manually setting variable.

  [ Tobias Hansen ]
  * New upstream version.
  * Remove patches (applied upstream):
    - no-install-docs.patch
    - linbox_charpoly_fullCRA.patch
    - gcc8.patch
  * Update Build-Depends:
    - fflas-ffpack (>= 2.4.3)
    - libgivaro-dev (>= 4.1.1)
  * Rename library package for soname change 1.5.2 -> 1.6.3.
  * Remove liblinboxsage packages.

 -- Tobias Hansen <thansen@debian.org>  Wed, 11 Sep 2019 20:44:18 +0200

linbox (1.5.2-2) unstable; urgency=medium

  [ Doug Torrance ]
  * debian/compat
    - Bump debhelper compatibility level to 11.
  * debian/control
    - Bump versioned dependency on debhelper to >= 11.
    - Bump Standards-Version to 4.1.5.
  * debian/patches/gcc8.patch
    - New patch from Arch; fixes build with GCC8 by removing unnecessary
      double template (Closes: #897801).
  * debian/watch
    - Use uscan v4 special strings.

  [ Tobias Hansen ]
  * Update doc-base file after documentation location changed due to compat 11.

 -- Doug Torrance <dtorrance@piedmont.edu>  Thu, 19 Jul 2018 22:22:30 -0400

linbox (1.5.2-1) unstable; urgency=medium

  * Team upload to unstable.

 -- Tobias Hansen <thansen@debian.org>  Tue, 26 Jun 2018 11:26:16 +0200

linbox (1.5.2-1~exp2) experimental; urgency=medium

  * Team upload.
  * Disable non-standard instructions sets.
  * Depend on latest fflas-ffpack revision.

 -- Tobias Hansen <thansen@debian.org>  Wed, 06 Jun 2018 14:56:13 +0200

linbox (1.5.2-1~exp1) experimental; urgency=medium

  [ Doug Torrance ]
  * New upstream release.
  * Refresh patches for new upstream version
  * Bump version in library sonames.
  * Bump version of fflas-ffpack in Build-Depends and Depends.
  * Update VCS-* to Salsa
  * Bump Standards-Version to 4.1.3.
  * New patch replacing probabilistic CRA for charpoly by a deterministic one.

  [ Tobias Hansen ]
  * Rename *.install files after packages have been renamed for SONAME bump.

 -- Doug Torrance <dtorrance@piedmont.edu>  Sat, 24 Mar 2018 18:41:34 -0400

linbox (1.4.2-5) unstable; urgency=medium

  * Team upload.

  [ Doug Torrance ]
  * debian/control
    - Drop libatlas-dev from Build-Depends (Closes: #871713).

  [ Sébastien Villemot ]
  * d/copyright: use https for Format URL.
  * d/control:
    - bump Standards-Version to 4.0.1.
    - fix URL in Vcs-Browser field.
    - fix typo in liblinbox-doc description.
  * Bump to debhelper compat level 10.
  * Effectively skip test-charpoly on mips and mipsel. Thanks to Radovan
    Birdic for the patch. (Closes: #856356)
  * d/rules: support the nodoc flag of DEB_BUILD_OPTIONS.

 -- Sébastien Villemot <sebastien@debian.org>  Sun, 20 Aug 2017 15:39:12 +0200

linbox (1.4.2-4) unstable; urgency=medium

  * debian/control
    - Switch build dependency on texlive-math-extra to texlive-science
      (Closes: #867094).

 -- Doug Torrance <dtorrance@piedmont.edu>  Mon, 03 Jul 2017 20:51:26 -0400

linbox (1.4.2-3) unstable; urgency=medium

  * Team upload.
  * Disable test-charpoly on mipsel. (Closes: #797167)

 -- Tobias Hansen <thansen@debian.org>  Tue, 29 Nov 2016 22:32:45 +0000

linbox (1.4.2-2) unstable; urgency=medium

  * Team upload.
  * Add versioned Build-Depends and Depends on fflas-ffpack. (Closes: #840457)
  * Disable building with fplll.

 -- Tobias Hansen <thansen@debian.org>  Mon, 07 Nov 2016 13:50:55 +0000

linbox (1.4.2-1) unstable; urgency=medium

  [ Doug Torrance ]
  * New upstream release.
    - Include modular-int32.h from new location, was moved from FFLAS-FFPACK
      to Givaro (Closes: #832539).
  * Tidy packaging with wrap-and-sort.
  * debian/compat
    - Bump debhelper compatibility level to 9.
  * debian/control
    - Add myself to Uploaders.
    - Bump versioned dependency on debhelper to >= 9.
    - Bump Standards-Version to 3.9.8.
    - Add Vcs-* fields (Closes: #723682).
  * debian/copyright
    - Update copyright information.
  * debian/liblinbox-dev.examples
    - Install contents of example directory, not examples directory itself.
      Avoids nested-examples-directory Lintian error.
  * debian/liblinbox-doc.lintian-overrides
    - New file; override Lintian warning for jQuery embedded by Doxygen.
  * debian/patches/no-install-docs.patch
    - New patch; do not include INSTALL in documentation.
  * debian/rules
    - Use all hardening flags.
  * debian/watch
    - Bump to version 4 and update download location.

  [ Tobias Hansen ]
  * Rename lib packages due to new soname.
  * Update build-depends for higher givaro version.
  * Refresh patches.
  * Add -fopenmp to LDFLAGS.
  * Install pkgconfig file.
  * New patch pkgconfig.patch:
    Do not put the CFLAGS used to build linbox into the pkg-config file.
  * Remove Lifeng Sun from Uploaders.
  * debian/patches/fix-gmp-detect.patch
    - Removed obsolete patch.
  * debian/patches/linboxsage-link-iml.patch
    - Removed obsolete patch.
  * Build-Depend on pkg-config.

 -- Doug Torrance <dtorrance@piedmont.edu>  Sun, 09 Oct 2016 14:54:43 +0100

linbox (1.3.2-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Fix "FTBFS: build failed on post-compile-test on mips/mipsel"
    Add fix-RR-RecCounter.patch
    Patch by Dejan Latinovic
    Closes: #733917

 -- Anibal Monsalve Salazar <anibal@debian.org>  Sun, 16 Feb 2014 05:27:42 +0000

linbox (1.3.2-1) unstable; urgency=low

  [ Lifeng Sun ]
  * New maintainer.
  * New upstream release. (Closes: #701176)
  * New packages: liblinboxsage1, liblinboxsage-dev and liblinbox-doc.
  * Use Debian specific SONAME: liblinbox-1.3.2.so.0 and
    liblinboxsage-1.3.2.so.0.
  * Bump Standards-Version to 3.9.4.
  * Support multiarch.
  * liblinbox-dev: ship examples files.
  * debian/control:
    - remove obsolte DM-Upload-Allowed field.
    - new build-deps: libm4ri-dev, libfplll-dev, libmpfr-dev, gnuplot,
      libgivaro1 (>= 3.7.1 && << 3.8.0), graphviz, texlive-math-extra,
      libm4rie-dev, texlive-fonts-recommended.
  * debian/patches:
    - add fix-gmp-detect.patch: fix GMP detection.
    - add multiarch-libntl.patch: fix NTL detection.
    - remove long-long-fixes.patch, ffpack.h.patch and build-system.patch:
      fixed by upstream.
  * debian/copyright: specify format version URI.
  * debian/rules: switch to debhelper tiny-style.
  * debian/source/format: specify source format to 3.0 (quilt).
  * Add debian/watch file.

  [ Tobias Hansen ]
  * debian/patches:
    - add linboxsage-link-iml.patch: Link liblinboxsage against libiml.

 -- Lifeng Sun <lifongsun@gmail.com>  Wed, 28 Aug 2013 23:02:25 +0800

linbox (1.1.6~rc0-4.1) unstable; urgency=low

  * Non-maintainer upload.
  * Don't ship .la files (Closes: #622448).
  * Switch to doxygen-latex (Closes: #616267).

 -- Luk Claes <luk@debian.org>  Sat, 25 Jun 2011 16:29:57 +0200

linbox (1.1.6~rc0-4) unstable; urgency=low

  * Convert to new CDBS aclocal macros API (Closes: #545588).

 -- Tim Abbott <tabbott@mit.edu>  Sun, 13 Sep 2009 22:54:30 -0400

linbox (1.1.6~rc0-3) unstable; urgency=low

  * Fix DEB_AUTO_UPDATE_LIBTOOL usage (Closes: #507198).

 -- Tim Abbott <tabbott@mit.edu>  Thu, 04 Dec 2008 21:53:12 -0500

linbox (1.1.6~rc0-2) unstable; urgency=low

  * Include missing dependency on liblapack-dev.
  * Include patch to workaround a bug in charpoly.
  * Disable the commentator functionality (this seems to break the
    minpoly, charpoly, and rank functions for dense integer matrices under
    some circumstances that include all usage by Sage).

 -- Tim Abbott <tabbott@mit.edu>  Sat, 26 Jul 2008 01:44:30 -0400

linbox (1.1.6~rc0-1) unstable; urgency=low

  * Initial Release (Closes: #480092).

 -- Tim Abbott <tabbott@mit.edu>  Wed, 18 Jun 2008 13:10:27 -0400
